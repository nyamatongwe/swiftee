# README #

### What is this repository for? ###

* This is a demonstration of using Scintilla from Swift.
* Version 0.4

![Swiftee.png](https://bitbucket.org/repo/7dAX6G/images/2928369925-Swiftee.png)

### How do I get set up? ###

* Download Scintilla from www.scintilla.org
* Apply patch from mailing list to Scintilla - this step will go away after the next release. 
* Load scintilla/cocoa/ScintillaFramework/ScintillaFramework.xcodeproj into Xcode
* Build
* Copy resulting Scintilla.framework into /Library/Frameworks
* Load Swiftee.xcodeproj into Xcode
* Press the go button

### Who do I talk to? ###

* Scintilla mailing list https://groups.google.com/forum/#!forum/scintilla-interest