//
//  main.swift
//  Swiftee
//
//  Created by Neil Hodgson on 5/06/2014.
//  Copyright (c) 2014 Neil Hodgson. All rights reserved.
//

import Cocoa

_ = NSApplicationMain(CommandLine.argc, CommandLine.unsafeArgv)
