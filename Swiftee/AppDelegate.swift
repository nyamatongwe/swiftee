//  AppDelegate.swift in Swiftee
//  Copyright 2014 Neil Hodgson.
//  This code is public domain.

import Cocoa
import Scintilla

class AppDelegate: NSObject, NSApplicationDelegate {

	@IBOutlet var window: NSWindow?
	var sci: ScintillaView?

	// sciMsg is just to avoid a lot of casts since Swift is picky and
	// a CInt can't be passed as a CLong without a cast.
	func sciMsg(_ m:CInt, wp:CInt=0, lp:CInt=0) {
		sci?.message(CUnsignedInt(m), wParam: CUnsignedLong(wp), lParam:CLong(lp))
	}

	func sciData(_ m:CInt, wp:CInt=0) -> [CChar] {
		if let s = sci {
			let cnt = s.message(CUnsignedInt(m), wParam: CUnsignedLong(m), lParam:0)
			let ret = [CChar](repeating: 0, count: cnt+1)	// +1 for potential NUL
			let umpRet = UnsafeMutablePointer<CChar>(mutating: ret);
			let umbpRet = UnsafeMutableBufferPointer(start: umpRet, count:ret.count)
			let ptrRet = unsafeBitCast(umbpRet.baseAddress, to: Int.self)
			s.message(CUnsignedInt(m), wParam: CUnsignedLong(wp), lParam:ptrRet)
			return ret
		} else {
			return [CChar]()
		}
	}

	func sciSet(_ m:CInt, wp:CInt=0, value: [CChar]) {
		if let s = sci {
			let ubp = UnsafeBufferPointer(start: value, count:value.count)
			let ptr = unsafeBitCast(ubp.baseAddress, to: Int.self)
			s.message(CUnsignedInt(m), wParam: CUnsignedLong(wp), lParam:ptr)
		}
	}

	func applicationDidFinishLaunching(_ aNotification: Notification) {
		if let wwin = window {
			sci = ScintillaView(frame: (wwin.contentView?.bounds)!)
			if let ssci = sci {
				ssci.autoresizingMask = NSAutoresizingMaskOptions(
                                  [.viewWidthSizable,.viewHeightSizable])
				wwin.contentView?.addSubview(ssci)
			}
		}
		sciMsg(SCI_STYLESETSIZE, wp:STYLE_DEFAULT, lp:15)
		sci?.setStringProperty(SCI_STYLESETFONT, parameter: CLong(STYLE_DEFAULT), value: "Papyrus")

		// Calling with character arrays [CChar]
		let bytes = Array("Menlo".utf8CString)
		sciSet(SCI_STYLESETFONT, wp:STYLE_DEFAULT, value:bytes)

		let byteRet = sciData(SCI_STYLEGETFONT, wp: STYLE_DEFAULT)
		if let fontName = String(cString: byteRet, encoding:String.Encoding.utf8) {
			NSLog(fontName)
		}

		sciMsg(SCI_STYLECLEARALL)
		sciMsg(SCI_SETEXTRAASCENT, wp:0)
		sciMsg(SCI_SETEXTRADESCENT, wp:0)
		sciMsg(SCI_SETLEXER, wp:SCLEX_CPP)
		sciMsg(SCI_STYLESETBOLD, wp:SCE_C_OPERATOR, lp:1)
		sciMsg(SCI_STYLESETFORE, wp:SCE_C_COMMENTLINE, lp:0x008000)
		sciMsg(SCI_STYLESETFORE, wp:SCE_C_WORD, lp:0x800000)
		sciMsg(SCI_STYLESETFORE, wp:SCE_C_WORD2, lp:0xA00060)
		sciMsg(SCI_STYLESETFORE, wp:SCE_C_STRING, lp:0x800080)
		sciMsg(SCI_STYLESETITALIC, wp:SCE_C_STRING, lp:1)
		sci?.setStringProperty(SCI_SETKEYWORDS, parameter: 0,
			value: "class func import let return var @IBOutlet")
		sci?.setStringProperty(SCI_SETKEYWORDS, parameter: 1,
			value: "CInt CLong CUnsignedInt CUnsignedLong")
		sci?.insertText("// Multiply by 5\nfunc 🌵🌺fŴÅༀཀུ(🌵: CInt) -> CInt {\n" +
			"\tlet gstr = \"The 🌺 is a large flower\"\n" +
			"\tlet ret = (🌵 + countElements(gstr) * 5;\n" +
			"\treturn ret + marginSize;\n" +
			"}\n")
		sciMsg(SCI_INDICSETSTYLE, wp:4, lp:INDIC_BOX)
		sciMsg(SCI_INDICSETFORE, wp:4, lp:0x0000A0)
		sciMsg(SCI_SETINDICATORCURRENT, wp:4)
		sciMsg(SCI_INDICATORFILLRANGE, wp:127, lp:19)
	}

	func applicationWillTerminate(_ aNotification: Notification) {
		// Insert code here to tear down your application
	}
}
